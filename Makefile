release:
	cd frontend && npx parcel build src/index.html
	cargo build --release

.PHONY: release
