use actix_web::{web, HttpResponse, Result};
use log::*;
use rocksdb::IteratorMode;
use serde::{Deserialize, Serialize};

use crate::DB;

pub fn configure_app(cfg: &mut web::RouterConfig) {
    cfg.service(web::resource("/tokens").route(web::get().to(list_tokens_handler)))
        .service(
            web::resource("/tokens/{name_or_token}")
                .route(web::post().to(create_token_handler))
                .route(web::delete().to(destroy_token_handler)),
        );
}

#[derive(Debug, Serialize, Deserialize)]
struct Token {
    name: String,
    token: String,
}

fn list_tokens_handler(db: web::Data<DB>) -> Result<HttpResponse> {
    let tokens: Vec<Token> = db
        .iterator(IteratorMode::Start)
        .filter_map(|(_, t)| serde_json::from_slice(&t).ok())
        .collect();
    Ok(HttpResponse::Ok().json(tokens))
}

fn create_token_handler(db: web::Data<DB>, path: web::Path<(String,)>) -> Result<HttpResponse> {
    let token = Token {
        name: path.0.clone(),
        token: nanoid::generate(64),
    };

    db.put(&token.token, serde_json::to_string(&token)?)
        .map_err(|err| {
            error!("DB insert error: {}", err);
            HttpResponse::InternalServerError().finish()
        })?;

    Ok(HttpResponse::Ok().json(token))
}

fn destroy_token_handler(db: web::Data<DB>, path: web::Path<(String,)>) -> Result<HttpResponse> {
    db.delete(&path.0).map_err(|err| {
        error!("DB delete error: {}", err);
        HttpResponse::InternalServerError().finish()
    })?;

    Ok(HttpResponse::Ok().finish())
}
