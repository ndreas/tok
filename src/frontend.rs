use std::borrow::Cow;

use actix_web::{dev::Body, web, HttpRequest, HttpResponse};
use mime_guess::guess_mime_type;
use rust_embed::RustEmbed;

pub fn configure_app(cfg: &mut web::RouterConfig) {
    cfg.service(web::resource("").to(index_handler))
        .service(web::resource("/").to(index_handler))
        .service(web::resource("/{_:.*}").to(asset_handler));
}

#[derive(RustEmbed)]
#[folder = "frontend/dist"]
struct Asset;

fn handle_embedded_file(path: &str) -> HttpResponse {
    match Asset::get(path) {
        Some(content) => {
            let body: Body = match content {
                Cow::Borrowed(b) => b.into(),
                Cow::Owned(b) => b.into(),
            };
            HttpResponse::Ok()
                .content_type(guess_mime_type(path).as_ref())
                .body(body)
        }
        None => HttpResponse::NotFound().finish(),
    }
}

fn index_handler() -> HttpResponse {
    handle_embedded_file("index.html")
}

fn asset_handler(req: HttpRequest) -> HttpResponse {
    let path = &req.path()[1..];
    handle_embedded_file(path)
}
