use actix_web::{http::header, web, HttpRequest, HttpResponse};

use crate::DB;

pub fn configure_app(cfg: &mut web::RouterConfig) {
    cfg.service(web::resource("/validate").to(validation_handler));
}

fn validation_handler(db: web::Data<DB>, req: HttpRequest) -> Result<(), HttpResponse> {
    let auth = req
        .headers()
        .get(header::AUTHORIZATION)
        .ok_or_else(|| HttpResponse::Unauthorized().finish())?
        .to_str()
        .map_err(|_| HttpResponse::Unauthorized().finish())?;
    let token = parse_authorization_header(auth)?;

    db.get_ref()
        .get(token)
        .map_err(|_| HttpResponse::Unauthorized().finish())?
        .ok_or_else(|| HttpResponse::Unauthorized().finish())?;

    Ok(())
}

fn parse_authorization_header(header: &str) -> Result<&str, HttpResponse> {
    let v: Vec<&str> = header.splitn(2, ' ').collect();
    match v.as_slice() {
        ["Bearer", tok] => Ok(tok),
        _ => Err(HttpResponse::Unauthorized().finish()),
    }
}
