use std::path::PathBuf;
use std::sync::Arc;

use ::log::*;
use actix_web::{middleware, App, HttpServer};
use failure::Fallible;
use structopt::StructOpt;

mod frontend;
mod log;
mod tokens;
mod validation;

pub type DB = Arc<rocksdb::DB>;

fn main() -> Fallible<()> {
    let opt = Opt::from_args();

    log::init(&opt.log_level, &opt.log_file)?;
    info!("Tok - serving tokens");

    let db: DB = Arc::new(rocksdb::DB::open_default(&opt.db)?);

    HttpServer::new(move || {
        App::new()
            .data(db.clone())
            .wrap(middleware::Logger::default())
            .configure(tokens::configure_app)
            .configure(validation::configure_app)
            .configure(frontend::configure_app)
    })
    .bind(&opt.listen)?
    .run()?;

    Ok(())
}

#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
struct Opt {
    #[structopt(
        long,
        default_value = "warn",
        raw(possible_values = r#"&["error", "warn", "info", "debug", "trace"]"#)
    )]
    /// Log level
    pub log_level: String,

    #[structopt(long)]
    /// Path to log file, defaults to stderr
    pub log_file: Option<PathBuf>,

    #[structopt(long, default_value = "127.0.0.1:3000")]
    /// IP and port to listen on
    pub listen: String,

    #[structopt(name = "DB")]
    /// Path to DB file
    pub db: PathBuf,
}
