export default interface Token {
    name: string
    token: string
}
